import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_chat/src/widgets/item_chat.dart';
import 'package:flutter_chat/src/models/user_chat.dart';
import 'package:flutter_chat/src/blocs/chat_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Chat extends StatefulWidget {

    const Chat(this.userChat);

    final UserChat userChat;
    static String chat = 'chat';

    @override
    ChatState createState() => ChatState(userChat);
}

class ChatState extends State<Chat> {

    ChatState(this.userChat);

    UserChat userChat;
    String groupChatId;

    ChatBloc bloc = ChatBloc();
    TextEditingController textEditingController = TextEditingController();

    @override
  void initState() {
      super.initState();
      groupChatId = '';
      if (userChat.currentId.hashCode <= userChat.documentSnapshot['id'].hashCode) {
          groupChatId = '${userChat.currentId}-${userChat.documentSnapshot['id']}';
      } else {
          groupChatId = '${userChat.documentSnapshot['id']}-${userChat.currentId}';
      }
      Firestore.instance.collection('users').document(userChat.currentId).updateData(
          <String, String>{'chattingWith': '${userChat.documentSnapshot['id'].toString()}'}
      );
  }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Text('Chat with ${userChat.documentSnapshot['nickname'].toString()}'),
            ),
            body: Column(
                children: <Widget>[
                    buildListMessage(),
                    buildInput()
                ],
            )
        );
    }

    Widget buildListMessage() {
        return Flexible(
            child:StreamBuilder<QuerySnapshot>(stream: Firestore.instance.collection('messages')
                .document(groupChatId)
                .collection(groupChatId)
                .orderBy('timestamp', descending: true)
                .limit(20)
                .snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (!snapshot.hasData) {
                    return const Center(
                        child: CircularProgressIndicator(),
                    );
                } else {
                    return ListView.builder(
                        padding: const EdgeInsets.all(10.0),
                        itemCount: snapshot.data.documents.length,
                        reverse: true,
                        itemBuilder: (BuildContext context, int index) {
                            if (!snapshot.hasData) {
                                return Container();
                            } else {
                                return ItemChat(userChat.currentId, snapshot.data.documents[index]);
                            }
                        },
                    );
                }
            })
        );

    }

    Widget buildInput() {
        return Container(
            width: double.infinity,
            height: 50.0,
            child: Row(
                children: <Widget>[
                    Flexible(
                        child: TextField(
                            controller: textEditingController,
                        )
                    ),
                    IconButton(
                        icon: Icon(Icons.send),
                        onPressed: () {
                            if (textEditingController.text.isNotEmpty) {
                                bloc.sendMessage(userChat.currentId, userChat.documentSnapshot['id'].toString(), groupChatId, textEditingController.text);
                                textEditingController.clear();
                            } else {
                                Fluttertoast.showToast(
                                    msg: 'Isi Chat',
                                    backgroundColor: Colors.red,
                                );
                            }
                        },
                    )
                ],
            ),
            decoration: BoxDecoration(
                border: Border(top: BorderSide(color: Colors.grey, width: 0.5)), color: Colors.white),
        );
    }
}