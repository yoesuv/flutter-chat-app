import 'package:flutter/material.dart';
import 'package:flutter_chat/src/screens/login.dart';

class SplashScreen extends StatelessWidget {

	@override
	Widget build(BuildContext context) {

			Future<void>.delayed(const Duration(seconds: 2), (){
					Navigator.pushNamedAndRemoveUntil(context, Login.login, ModalRoute.withName('/'));
			});

			return Scaffold(
					body: Container(
							color: Colors.teal,
							child: const Center(
								child: Text('Flutter Chat', style: TextStyle(
										color: Colors.white,
										fontSize: 20.0
								))
							)
					)
			);
	}

}