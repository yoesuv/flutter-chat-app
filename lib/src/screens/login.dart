import 'package:flutter/material.dart';
import 'package:flutter_chat/src/blocs/login_bloc.dart';
import 'package:flutter_chat/src/models/user_model.dart';
import 'package:flutter_chat/src/screens/home.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Login extends StatefulWidget {

    static const String login = 'login';

    @override
    LoginState createState() => LoginState();
}

class LoginState extends State<Login> {

    LoginBloc bloc = LoginBloc();

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: const Text('Login Chat')
            ),
            body: Center(
                child: RaisedButton(
                    child: const Text('Login'),
                    onPressed: () {
                        final Future<UserModel> result = bloc.handleSignIn();
                        result.then((UserModel result) {
                            if (result.loginSuccess) {
                                Fluttertoast.showToast(
                                    msg: 'Login Sukses',
                                    backgroundColor: Colors.green,
                                    textColor: Colors.white
                                );
                                Navigator.pushNamedAndRemoveUntil(context, Home.home, ModalRoute.withName('/'), arguments: result);
                            } else {
                                Fluttertoast.showToast(
                                    msg: 'Login Gagal',
                                    backgroundColor: Colors.red,
                                    textColor: Colors.white
                                );
                            }
                        });
                    }
                )
            )
        );
    }

}