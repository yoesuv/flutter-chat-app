import 'package:flutter/material.dart';
import 'package:flutter_chat/src/blocs/home_bloc.dart';
import 'package:flutter_chat/src/models/user_model.dart';
import 'package:flutter_chat/src/widgets/item_contact.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Home extends StatefulWidget {

    const Home(this.userModel);

    static const String home = 'home';
    final UserModel userModel;

    @override
    HomeState createState() => HomeState(userModel);

}

class HomeState extends State<Home> {

    HomeState(this.userModel);

    HomeBloc bloc = HomeBloc();
    UserModel userModel;

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Text('Daftar Kontak ${userModel.nickName}'),
                actions: <Widget>[
                    IconButton(
                        icon: Icon(Icons.power_settings_new),
                        onPressed: () {
                            dialogLogout(context);
                        },
                    )
                ],
            ),
            body: buildListContact()
        );
    }

    Widget buildListContact() {
        return StreamBuilder<QuerySnapshot>(
            stream: Firestore.instance.collection('users').snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (!snapshot.hasData) {
                    return const Center(
                        child: CircularProgressIndicator(),
                    );
                } else {
                    return ListView.builder(
                        padding: const EdgeInsets.all(8.0),
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (BuildContext context, int index) {
                            return ItemContact(userModel.id, snapshot.data.documents[index]);
                        }
                    );
                }
            }
        );
    }

    void dialogLogout(BuildContext context) {
        showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
                return AlertDialog(
                    title: const Text('Logout'),
                    content: const Text('Logout dari Aplikasi'),
                    actions: <Widget>[
                        FlatButton(
                            child: const Text('Batal'),
                            onPressed: () {
                                Navigator.pop(context);
                            },
                        ),
                        FlatButton(
                            child: const Text('Ok'),
                            onPressed: () {
                                Navigator.pop(context);
                                bloc.handleSignOut(context);
                            },
                        ),
                    ],
                );
            }
        );
    }
}