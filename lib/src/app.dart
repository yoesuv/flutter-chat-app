import 'package:flutter/material.dart';
import 'package:flutter_chat/src/blocs/chat_bloc.dart';
import 'package:flutter_chat/src/blocs/home_bloc.dart';
import 'package:flutter_chat/src/blocs/login_bloc.dart';
import 'package:flutter_chat/src/routes/routes.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class App extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return MultiProvider(
            providers: <SingleChildWidget>[
                Provider<ChatBloc>(create:(_) => ChatBloc()),
                Provider<HomeBloc>(create: (_) => HomeBloc()),
                Provider<LoginBloc>(create: (_) => LoginBloc()),
            ],
            child: MaterialApp(
                title: 'Flutter Chat',
                theme: ThemeData(
                    primarySwatch: Colors.teal,
                ),
                onGenerateRoute: Routes.routes,
            )
        );
    }

}
