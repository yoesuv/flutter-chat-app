import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ItemChat extends StatelessWidget {

    const ItemChat(this.currentId, this.documentSnapshot);

    final String currentId;
    final DocumentSnapshot documentSnapshot;

    @override
    Widget build(BuildContext context) {
        return Container(
            padding: const EdgeInsets.all(10.0),
            child: buildContent()
        );
    }

    Widget buildContent() {
        if (currentId == documentSnapshot['idFrom']) {
            return Container(
                child: Text(documentSnapshot['content'].toString(),
                    style: const TextStyle(
                        fontSize: 16
                    )
                )
            );
        } else {
          return Container(
              child: Text(documentSnapshot['content'].toString(),
                  textAlign: TextAlign.end,
                  style: const TextStyle(
                        fontSize: 16,
                        color: Colors.teal,
                  )
              )
          );
        }
    }

}