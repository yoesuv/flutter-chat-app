import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_chat/src/models/user_chat.dart';
import 'package:flutter_chat/src/screens/chat.dart';

class ItemContact extends StatelessWidget {

    const ItemContact([this.currentId, this.documentSnapshot]);

    final String currentId;
    final DocumentSnapshot documentSnapshot;

    @override
    Widget build(BuildContext context) {
        if (documentSnapshot['id'] == currentId) {
            return Container();
        } else {
            return Container(
                child: Row(
                    children: <Widget>[
                        CachedNetworkImage(
                            imageUrl: documentSnapshot['photoUrl'].toString(),
                            fit: BoxFit.cover,
                            height: 50.0,
                            width: 50.0,
                        ),
                        FlatButton(
                            child: Text(documentSnapshot['nickname'].toString(),
                                style: const TextStyle(
                                    fontSize: 18.0
                                )
                            ),
                            onPressed: () {
                                final UserChat userChat = UserChat(currentId, documentSnapshot);
                                Navigator.pushNamed(context, Chat.chat, arguments: userChat);
                            }
                        )
                    ],
                )

            );
        }
    }

}
