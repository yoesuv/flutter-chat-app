import 'package:flutter/material.dart';
import 'package:flutter_chat/src/models/user_model.dart';
import 'package:flutter_chat/src/screens/chat.dart';
import 'package:flutter_chat/src/screens/home.dart';
import 'package:flutter_chat/src/screens/login.dart';
import 'package:flutter_chat/src/screens/splash.dart';
import 'package:flutter_chat/src/models/user_chat.dart';

class Routes {

    static Route<dynamic> routes(RouteSettings settings) {
        if (settings.name == '/') {
            return MaterialPageRoute<dynamic>(
                builder: (BuildContext context) {
                    return SplashScreen();
                }
            );
        } else if (settings.name == Login.login) {
            return MaterialPageRoute<dynamic>(
                builder: (BuildContext context) {
                    return Login();
                }
            );
        } else if(settings.name == Chat.chat) {
            final UserChat userChat = settings.arguments as UserChat;
            return MaterialPageRoute<dynamic>(
                builder: (BuildContext context) {
                    return Chat(userChat);
                }
            );
        }  else {
            final UserModel model = settings.arguments as UserModel;
            return MaterialPageRoute<dynamic>(
                builder: (BuildContext context) {
                    return Home(model);
                }
            );
        }
    }

}
