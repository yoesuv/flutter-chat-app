class UserModel {

    UserModel([this.id, this.nickName, this.photoUrl, this.loginSuccess]);

    String id;
    String nickName;
    String photoUrl;
    bool loginSuccess;
}
