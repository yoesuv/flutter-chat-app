import 'package:cloud_firestore/cloud_firestore.dart';

class UserChat {

    UserChat([this.currentId, this.documentSnapshot]);

    String currentId;
    DocumentSnapshot documentSnapshot;

}