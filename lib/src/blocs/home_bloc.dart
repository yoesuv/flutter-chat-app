import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_chat/src/screens/login.dart';

class HomeBloc {

    GoogleSignIn googleSignIn = GoogleSignIn();

    void handleSignOut(BuildContext context) {
        FirebaseAuth.instance.signOut();
        googleSignIn.disconnect();
        googleSignIn.signOut();
        Navigator.pushNamedAndRemoveUntil(context, Login.login, ModalRoute.withName('/'));
    }
    
}
