import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_chat/src/models/user_model.dart';

class LoginBloc {

    final GoogleSignIn googleSignIn = GoogleSignIn();
    final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

    Future<UserModel> handleSignIn() async{
        final GoogleSignInAccount account = await googleSignIn.signIn();
        final GoogleSignInAuthentication auth = await account.authentication;
        final AuthCredential credential = GoogleAuthProvider.getCredential(
            accessToken: auth.accessToken,
            idToken: auth.idToken
        );
        final FirebaseUser firebaseUser = (await firebaseAuth.signInWithCredential(credential)).user;
        if (firebaseUser!=null) {
            final QuerySnapshot result = await Firestore.instance.collection('users').where('id', isEqualTo: firebaseUser.uid).getDocuments();
            final List<DocumentSnapshot> documents = result.documents;
            if (documents.isEmpty) {
                final Map<String, dynamic> dataUser = <String, String>{'nickname': firebaseUser.displayName, 'photoUrl': firebaseUser.photoUrl, 'id': firebaseUser.uid};
                Firestore.instance.collection('users').document(firebaseUser.uid).setData(dataUser);
                return UserModel(firebaseUser.uid, firebaseUser.displayName, firebaseUser.photoUrl, true);
            } else {
                return UserModel(documents[0]['id'].toString(), documents[0]['nickname'].toString(), documents[0]['photoUrl'].toString(), true);
            }
        } else {
            return UserModel('','','', false);
        }
    }

}