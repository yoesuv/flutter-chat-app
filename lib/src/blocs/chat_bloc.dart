import 'package:cloud_firestore/cloud_firestore.dart';

class ChatBloc {

    void sendMessage(String idFrom, String idTo, String groupChatId, String chat) {
        final DocumentReference documentReference  = Firestore.instance.collection('messages')
            .document(groupChatId)
            .collection(groupChatId)
            .document(DateTime.now().millisecondsSinceEpoch.toString());

        Firestore.instance.runTransaction((Transaction transaction) async{
            await transaction.set(documentReference, <String, String>{
                'idFrom': idFrom,
                'idTo' : idTo,
                'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),
                'content': chat
            });
        });
    }

}