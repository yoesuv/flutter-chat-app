## Flutter Realtime Chat 1.0.0 ##

- :sparkles: initial release

Download APK File [Here](https://www.dropbox.com/s/6oo7je0bwls4ype)

#### Flutter Dependencies ####
```
Flutter 1.12.13+hotfix.5 • channel stable • https://github.com/flutter/flutter.git
Framework • revision 27321ebbad (2 days ago) • 2019-12-10 18:15:01 -0800
Engine • revision 2994f7e1e6
Tools • Dart 2.7.0
```

#### ScreenShot ####
| Android | Android | iOS | iOS |
| :---: | :---: | :---: | :---: |
| ![List Contact Android](https://i.imgur.com/lueVI6y.jpg) |  ![List Chat Android](https://i.imgur.com/tkX8EUK.jpg) |  ![List Contact iOS](https://i.imgur.com/PpEhAxX.png) | ![List Chat iOS](https://i.imgur.com/FYoUxaQ.png) |

#### List Libraries ####
- [Cached Network Image](https://pub.dev/packages/cached_network_image)
- [Flutter Toast](https://pub.dev/packages/fluttertoast)
- [Provider](https://pub.dev/packages/provider)
- [RxDart](https://pub.dev/packages/rxdart)
- [Google Sign In](https://pub.dev/packages/google_sign_in)
- [Firebase Core](https://pub.dev/packages/firebase_core)
- [Firebase Auth](https://pub.dev/packages/firebase_auth)
- [Firebase Analytics](https://pub.dev/packages/firebase_analytics)
- [Firebase Storage](https://pub.dev/packages/firebase_storage)
- [Cloud Firestore](https://pub.dev/packages/cloud_firestore)

### Reference ###
- [Medium Article](https://medium.com/flutter-community/building-a-chat-app-with-flutter-and-firebase-from-scratch-9eaa7f41782e)
